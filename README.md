Small improvements:

    - add double fork mechanism to tint_exec function.
      Before this, each application started from a launcher 
      was connected with a separate '/usr/bin/sh' process residing in memory.
      Now this processes will be terminated after launching the main application.

    - add 'Always on top' property to the autohiding panel. 
      Now when the panel will be showing, it will cover other windows behind it.
      
    - fixing blinking tooltips
      
    - modification of the `skip taskbar` hint handling
      
    - killing zombie processes. In my case zombies were created for executors with the continuous output,
      after refreshing the panel configuration
    
    
    
